package com.kiseru.asteroids.server.model

import com.kiseru.asteroids.server.exception.GameFinishedException
import com.kiseru.asteroids.server.service.CourseCheckerService
import java.util.UUID

class Spaceship(
    val id: UUID,
    private val owner: ApplicationUser,
    val room: Room,
    private val courseCheckerService: CourseCheckerService,
    x: Int,
    y: Int,
) : Point(x, y) {

    override val symbolToShow: String
        get() = owner.id.toString()

    override val type: Type = Type.SPACESHIP

    val isAsteroidInFrontOf
        get() = courseCheckerService.isAsteroid()

    val isGarbageInFrontOf
        get() = courseCheckerService.isGarbage()

    val isWallInFrontOf
        get() = courseCheckerService.isWall()

    var direction = Direction.UP

    override fun destroy() {
        throw UnsupportedOperationException()
    }

    fun subtractScore() {
        if (room.isGameFinished) {
            throw GameFinishedException()
        }

        owner.score -= 50
        if (owner.score < 0) {
            owner.isAlive = false
        }
    }

    fun addScore() {
        if (room.isGameFinished) {
            throw GameFinishedException()
        }

        owner.score += 10
    }

    fun checkCollectedGarbage(collected: Int) {
        if (collected >= room.game.garbageNumber) {
            room.setGameFinished()
        }
    }
}
