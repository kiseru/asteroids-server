package com.kiseru.asteroids.server.model

/**
 * Направления движения космического корабля.
 */
enum class Direction {

    /**
     * Вверх.
     */
    UP,

    /**
     * Вниз.
     */
    DOWN,

    /**
     * Влево.
     */
    LEFT,

    /**
     * Вправо.
     */
    RIGHT,
}
