package com.kiseru.asteroids.server.direction

import com.kiseru.asteroids.server.model.Direction
import com.kiseru.asteroids.server.model.Point
import com.kiseru.asteroids.server.model.Screen

interface DirectionHandler {

    val direction: Direction

    fun go(point: Point)

    fun rollback(point: Point)

    fun isWall(point: Point, screen: Screen): Boolean

    fun checkContaining(point: Point, coordinates: List<Point>): Boolean
}

