package com.kiseru.asteroids.server.direction.impl

import com.kiseru.asteroids.server.direction.DirectionHandler
import com.kiseru.asteroids.server.model.Direction
import com.kiseru.asteroids.server.model.Point
import com.kiseru.asteroids.server.model.Screen
import org.springframework.stereotype.Component

@Component
class DownDirectionHandler : DirectionHandler {

    override val direction: Direction = Direction.DOWN

    override fun go(point: Point) {
        point.y += 1
    }

    override fun rollback(point: Point) {
        point.y -= 1
    }

    override fun isWall(point: Point, screen: Screen): Boolean = point.y == screen.height

    override fun checkContaining(point: Point, coordinates: List<Point>): Boolean =
        coordinates.any { it.x == point.x && it.y == point.y + 1 }
}

