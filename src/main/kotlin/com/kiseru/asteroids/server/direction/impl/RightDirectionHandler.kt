package com.kiseru.asteroids.server.direction.impl

import com.kiseru.asteroids.server.direction.DirectionHandler
import com.kiseru.asteroids.server.model.Direction
import com.kiseru.asteroids.server.model.Point
import com.kiseru.asteroids.server.model.Screen
import org.springframework.stereotype.Component

@Component
class RightDirectionHandler : DirectionHandler {

    override val direction: Direction = Direction.RIGHT

    override fun go(point: Point) {
        point.x += 1
    }

    override fun rollback(point: Point) {
        point.x -= 1
    }

    override fun isWall(point: Point, screen: Screen): Boolean = point.x == screen.width

    override fun checkContaining(point: Point, coordinates: List<Point>): Boolean =
        coordinates.any { it.x == point.x + 1 && it.y == point.y }
}