package com.kiseru.asteroids.server.direction.impl

import com.kiseru.asteroids.server.direction.DirectionHandler
import com.kiseru.asteroids.server.direction.DirectionHandlerFactory
import com.kiseru.asteroids.server.model.Direction
import org.springframework.stereotype.Component

@Component
class DirectionHandlerFactoryImpl(
    val handlers: Collection<DirectionHandler>,
) : DirectionHandlerFactory {

    override fun createHandler(direction: Direction): DirectionHandler =
        handlers.first { it.direction == direction }
}