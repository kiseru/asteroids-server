package com.kiseru.asteroids.server.direction

import com.kiseru.asteroids.server.model.Direction

interface DirectionHandlerFactory {

    fun createHandler(direction: Direction): DirectionHandler
}