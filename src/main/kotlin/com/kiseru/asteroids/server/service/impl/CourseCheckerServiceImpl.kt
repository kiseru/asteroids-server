package com.kiseru.asteroids.server.service.impl

import com.kiseru.asteroids.server.direction.DirectionHandlerFactory
import com.kiseru.asteroids.server.model.Point
import com.kiseru.asteroids.server.model.Screen
import com.kiseru.asteroids.server.model.Spaceship
import com.kiseru.asteroids.server.model.Type
import com.kiseru.asteroids.server.service.CourseCheckerService

class CourseCheckerServiceImpl(
    private val directionHandlerFactory: DirectionHandlerFactory,
    private val pointsOnMap: List<Point>,
    private val screen: Screen,
) : CourseCheckerService {

    lateinit var spaceship: Spaceship

    override fun isAsteroid(): Boolean {
        val handler = directionHandlerFactory.createHandler(spaceship.direction)
        return pointsOnMap.filter { it.type == Type.ASTEROID }
            .let { handler.checkContaining(spaceship, it) }
    }

    override fun isGarbage(): Boolean {
        val handler = directionHandlerFactory.createHandler(spaceship.direction)
        return pointsOnMap.filter { it.type == Type.GARBAGE }
            .let { handler.checkContaining(spaceship, it) }
    }

    override fun isWall(): Boolean {
        val handler = directionHandlerFactory.createHandler(spaceship.direction)
        return handler.isWall(spaceship, screen)
    }
}