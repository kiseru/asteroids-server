package com.kiseru.asteroids.server.spaceship.impl

import com.kiseru.asteroids.server.coordinate.CoordinateService
import com.kiseru.asteroids.server.direction.DirectionHandlerFactory
import com.kiseru.asteroids.server.model.ApplicationUser
import com.kiseru.asteroids.server.model.Game
import com.kiseru.asteroids.server.model.Room
import com.kiseru.asteroids.server.model.Spaceship
import com.kiseru.asteroids.server.repository.SpaceshipRepository
import com.kiseru.asteroids.server.service.impl.CourseCheckerServiceImpl
import com.kiseru.asteroids.server.spaceship.SpaceshipService
import org.springframework.stereotype.Service
import java.util.UUID

@Service
class SpaceshipServiceImpl(
    private val coordinateService: CoordinateService,
    private val directionHandlerFactory: DirectionHandlerFactory,
    private val spaceshipRepository: SpaceshipRepository,
) : SpaceshipService {

    override suspend fun createSpaceship(user: ApplicationUser, room: Room, game: Game): Spaceship {
        val courseCheckerService = CourseCheckerServiceImpl(
            directionHandlerFactory,
            game.pointsOnScreen,
            game.screen,
        )
        val (x, y) = coordinateService.generateUniqueRandomCoordinates(game)
        val spaceship = spaceshipRepository.create(user, room, courseCheckerService, x, y)
        user.spaceshipId = spaceship.id
        courseCheckerService.spaceship = spaceship
        return spaceship
    }

    override suspend fun findSpaceshipById(spaceshipId: UUID): Spaceship? =
        spaceshipRepository.findById(spaceshipId)
}
