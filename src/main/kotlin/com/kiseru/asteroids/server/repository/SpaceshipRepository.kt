package com.kiseru.asteroids.server.repository

import com.kiseru.asteroids.server.model.ApplicationUser
import com.kiseru.asteroids.server.model.Room
import com.kiseru.asteroids.server.model.Spaceship
import com.kiseru.asteroids.server.service.CourseCheckerService
import java.util.UUID

interface SpaceshipRepository {

    suspend fun create(
        user: ApplicationUser,
        room: Room,
        courseCheckerService: CourseCheckerService,
        x: Int,
        y: Int,
    ): Spaceship

    suspend fun findById(spaceshipId: UUID): Spaceship?
}