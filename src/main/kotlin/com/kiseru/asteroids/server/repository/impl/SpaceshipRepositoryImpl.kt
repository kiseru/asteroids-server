package com.kiseru.asteroids.server.repository.impl

import com.kiseru.asteroids.server.model.ApplicationUser
import com.kiseru.asteroids.server.model.Room
import com.kiseru.asteroids.server.model.Spaceship
import com.kiseru.asteroids.server.repository.SpaceshipRepository
import com.kiseru.asteroids.server.service.CourseCheckerService
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import org.springframework.stereotype.Repository
import java.util.UUID

@Repository
class SpaceshipRepositoryImpl : SpaceshipRepository {

    private val mutex = Mutex()

    private val storage = mutableMapOf<UUID, Spaceship>()

    override suspend fun create(
        user: ApplicationUser,
        room: Room,
        courseCheckerService: CourseCheckerService,
        x: Int,
        y: Int,
    ): Spaceship =
        mutex.withLock {
            val spaceshipId = generateSpaceshipId()
            val spaceship = Spaceship(spaceshipId, user, room, courseCheckerService, x, y)
            storage[spaceshipId] = spaceship
            spaceship
        }

    private fun generateSpaceshipId(): UUID =
        generateSequence { UUID.randomUUID() }
            .dropWhile { storage.containsKey(it) }
            .first()

    override suspend fun findById(spaceshipId: UUID): Spaceship? =
        mutex.withLock { storage[spaceshipId] }
}