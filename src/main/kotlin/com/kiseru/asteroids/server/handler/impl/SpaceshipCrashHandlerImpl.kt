package com.kiseru.asteroids.server.handler.impl

import com.kiseru.asteroids.server.direction.DirectionHandlerFactory
import com.kiseru.asteroids.server.handler.SpaceshipCrashHandler
import com.kiseru.asteroids.server.model.Game
import com.kiseru.asteroids.server.model.Point
import com.kiseru.asteroids.server.model.Spaceship
import com.kiseru.asteroids.server.model.Type
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock

class SpaceshipCrashHandlerImpl(
    private val directionHandlerFactory: DirectionHandlerFactory,
    private val game: Game,
    private val spaceship: Spaceship,
) : SpaceshipCrashHandler {

    private val mutex = Mutex()

    override suspend fun check() {
        val pointsOnScreen = game.pointsOnScreen
        val collisionPoint = pointsOnScreen.firstOrNull {
            it.type != Type.SPACESHIP && it.isVisible && it.x == spaceship.x && it.y == spaceship.y
        }
        checkSpaceshipCrashing(collisionPoint)
    }

    private suspend fun checkSpaceshipCrashing(collisionPoint: Point?) {
        if (collisionPoint != null) {
            crash(spaceship, collisionPoint.type)
            collisionPoint.destroy()
        } else if (spaceship.x !in 1..game.screen.width || spaceship.y !in 1..game.screen.height) {
            crash(spaceship, Type.WALL)
        }
    }

    private suspend fun crash(spaceship: Spaceship, type: Type) =
        mutex.withLock {
            when {
                type === Type.ASTEROID -> {
                    spaceship.subtractScore()
                }
                type === Type.GARBAGE -> {
                    spaceship.addScore()
                    val collected = spaceship.room.game.incrementCollectedGarbageCount()
                    spaceship.checkCollectedGarbage(collected)
                }
                type === Type.WALL -> {
                    val directionHandler = directionHandlerFactory.createHandler(spaceship.direction)
                    directionHandler.rollback(spaceship)
                    spaceship.subtractScore()
                }
            }
        }
}